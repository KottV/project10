
#include "Goniometer.h"

void Goniometer::resized()
{
    bounds = getLocalBounds();
    center = bounds.getCentre().toFloat();
    widthH = getWidth() / 2 ;
    heightH = getHeight() / 2;
}

void Goniometer::paint(juce::Graphics& g)
{
    float r = getWidth() / 2.6f;
    const auto pi = MathConstants<float>::pi;
    const auto pi025 = pi / 4;
    
    g.setColour (findColour(PFM10LookAndFeel::stripeColourId));
    g.drawEllipse (center.getX() - r, center.getY() - r, r * 2, r * 2, 1);
    
    for (float f = 0; f < 2 * pi; f = f + pi025)
    {
        g.drawLine(juce::Line (center, center.getPointOnCircumference(r, f)));
    }
    
    g.setFont(18);


    juce::Rectangle<float> rect(0, 0, 22, 22);
    
    Array<String> labels{"+S", "L", "M", "R", "-S"};
    for (int i = 0; i < labels.size(); ++i)
    {
        rect.setCentre(center.getPointOnCircumference(r + 12, pi025 * (6 + i)));
        g.drawFittedText(labels.getReference(i), rect.getSmallestIntegerContainer(), juce::Justification::centred, 1, 1.0f);
    }
    
    g.setColour(findColour(PFM10LookAndFeel::curveColourId));
    g.strokePath(p, juce::PathStrokeType(2.f));
}

void Goniometer::update(const juce::AudioBuffer<float>& buffer)
{
    p.clear();

    x = (buffer.getSample(0, 0) - buffer.getSample(1, 0)) * dbCoef;
    y = (buffer.getSample(0, 0) + buffer.getSample(1, 0)) * dbCoef;
        
    p.startNewSubPath(center.x - widthH * x, center.y - heightH * y);
   
    for (int i = 0; i < buffer.getNumSamples(); ++i)
    {
        x = (buffer.getSample(0, i) - buffer.getSample(1, i)) * dbCoef;
        y = (buffer.getSample(0, i) + buffer.getSample(1, i)) * dbCoef;
        p.lineTo(center.x - widthH * x, center.y - heightH * y);
    }
    
    //DBG("x: " << x << " y : " << y << "\n");
    repaint();
}

