#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

template<typename T>
struct CircularBuffer
{
    using DataType = std::vector<T>;

    void resize(std::size_t s, T fillValue)
    {
        circularBuffer.resize(s, fillValue);
        writeIndex = 0;
    }
        
    void clear(T fillValue)
    {
        circularBuffer.assign(getSize(), fillValue);
    }

    void write(T t)
    {
        auto localIndex = writeIndex.load();
        circularBuffer[localIndex] = t;
        ++localIndex;
        if (localIndex == circularBuffer.size())
        {
            localIndex = 0;
        }
        writeIndex.store(localIndex);
    }

    DataType& getData()
    {
        return circularBuffer;
    }

    size_t getReadIndex() const
    {
        return writeIndex;
    }

    size_t getSize() const
    {
        return circularBuffer.size();
    }

private:
    DataType circularBuffer;
    std::atomic<size_t> writeIndex{ 0 };
};
