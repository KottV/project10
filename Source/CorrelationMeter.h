#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Averager.h"
#include "PFM10LookAndFeel.h"
#include <math.h>

#define NEGATIVE_INFINITY -100.0f
#define MAX_DECIBELS 6.0f

struct CorrelationMeter : juce::Component
{
    template <typename Type>
    using Filter = juce::dsp::IIR::Filter<Type>;
    float cInst;
    ColourGradient colourGradient;

    CorrelationMeter(double sampleRate)
    {
        auto coefficients = juce::dsp::IIR::Coefficients<float>::makeLowPass(sampleRate, 67.f);
        
        filterL = Filter<float> (coefficients);
        filterR = Filter<float> (coefficients);
        filterC = Filter<float> (coefficients);
    }
    
    void paint(juce::Graphics& g) override;
    void update(const juce::AudioBuffer<float>& buffer);
    void resized() override;

private:
        
    Averager<float> correlationAveragerA{ 640, 0 };
    Averager<float> correlationAveragerB{ 3840, 0 };
    float dbLevel = NEGATIVE_INFINITY;
    Filter<float> filterL, filterR, filterC;
    juce::String str;
    juce::Rectangle< int > bounds;
    int x, w, yInst, yAver;
};
