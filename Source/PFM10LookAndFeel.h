// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2023 Konstantin <email>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"


struct ColorSet {
    juce::String setName{};
    juce::Colour meterMin;
    juce::Colour meterMax;
    juce::Colour stripe;
    juce::Colour curve;
    juce::Colour goniGround;
};

class PFM10LookAndFeel :  public LookAndFeel_V4
{
public:

    enum CustomColourIds
    {
        meterMinColourId = 0x1000,
        meterMaxColourId = 0x1001,
        stripeColourId = 0x1002,
        curveColourId = 0x1003,
        goniGroundId = 0x1004
    };

    PFM10LookAndFeel()
    {
        setColour(juce::Slider::thumbColourId, juce::Colours::red);
        // updateColors(darkSet);
        // setColourScheme(LookAndFeel_V4::getMidnightColourScheme());
    }
    void drawLinearSlider (Graphics& g, int x, int y, int width, int height,
                                       float sliderPos,
                                       float minSliderPos,
                                       float maxSliderPos,
                                       const Slider::SliderStyle style, Slider& slider) override
    {
        const float fx = (float) x, fy = (float) y, fw = (float) width, fh = (float) height;
        g.setColour (juce::Colours::red);
        Path p;
        p.addRectangle (fx, sliderPos, fw, 2.0f);
        g.fillPath (p);

    }

    void updateColors (const ColorSet colorSet)
    {
        setColour(meterMinColourId, colorSet.meterMin);
        setColour(meterMaxColourId, colorSet.meterMax);
        setColour(stripeColourId, colorSet.stripe);
        setColour(curveColourId, colorSet.curve);
        setColour(goniGroundId, colorSet.goniGround);
    }

    void switchTheme(int themeId)
    {
        if (themeId == 1)
        {
            updateColors(lightSet);
            setColourScheme(LookAndFeel_V4::getLightColourScheme());
        }
        else
        {
            updateColors(darkSet);
            setColourScheme(LookAndFeel_V4::getMidnightColourScheme());
        }
    }


    const ColorSet lightSet = {
       .setName = "lightSet",
       .meterMin = juce::Colour(136, 173, 67),
       .meterMax = juce::Colour(159, 173, 67),
       .stripe = juce::Colours::black,
       .curve = juce::Colours::darkkhaki,
       .goniGround = juce::Colours::lightgrey
    };

    const ColorSet darkSet = {
       .setName = "darkSet",
       .meterMin = juce::Colour(27,122,177),
       .meterMax = juce::Colour(184,61,61),
       .stripe = juce::Colours::grey,
       .curve = juce::Colours::green,
       .goniGround = juce::Colours::black
    };
};


