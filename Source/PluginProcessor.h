/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//#define WITH_DBSCALE

template <typename T>
struct Fifo
{
    void prepare(int numSamples, int numChannels)
    {
        for (auto& buffer : buffers)
        {
            buffer.setSize(numChannels, numSamples, true, true, true);
        }
    }
    
    bool push(const T& itemToAdd)
    {
        auto write = fifo.write(1);
        if (write.blockSize1 >= 1)
        {
            buffers[write.startIndex1] = itemToAdd;
            return true;
        }
        return false;
    }
    bool pull(T& itemToUpdate)
    {
        auto read = fifo.read(1);
        if (read.blockSize1 >= 1)
        {
            itemToUpdate = buffers[read.startIndex1];
//             DBG("we are here");
            return true;
        }
        return false;
    }

private:
    static constexpr int Capacity = 5;
    std::array<T, Capacity> buffers;
    AbstractFifo fifo{ Capacity };

};

//==============================================================================
/**
*/
class Pfmcpp_project10AudioProcessor  : public AudioProcessor
{
public:
    Fifo<AudioBuffer<float>> fifo;
    int numSamples;
    //==============================================================================
    Pfmcpp_project10AudioProcessor();
    ~Pfmcpp_project10AudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //==============================================================================

    const char* pluginVersionString = ProjectInfo::versionString;
    juce::ValueTree PFM10ValueTree;

    static const juce::Identifier pluginVersion, decayRateValue, holdButtonValue, metersSelectValue, sliderPeakValue,
    sliderRMSValue, holdDurationValue, averagerDurationValue, zoomValue, histViewValue, themeSelectValue;

    bool oldversionstate = false;


private:
    //==============================================================================
#if defined(WITH_DBSCALE)
    juce::dsp::Oscillator<float> osc { [] (float x) {return std::sin(x);}};
    juce::dsp::Gain<float> gainDb;
#endif
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Pfmcpp_project10AudioProcessor)
};

