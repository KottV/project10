#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include <math.h>
#include "PFM10LookAndFeel.h"

struct Goniometer : juce::Component
{
    void paint(juce::Graphics& g) override;
    void resized() override;
    void update(const juce::AudioBuffer<float>& buffer);
    void gmeterZoom(float dbZoom)
    {
        dbCoef= dbCoefConst * dbZoom;
    }

private:
    float x = 0, y = 0;
    juce::Path p;
    const float dbCoefConst = juce::Decibels::decibelsToGain(-3.f);
    float dbCoef = dbCoefConst;
    juce::Rectangle<int> bounds;
    juce::Point<float> center;
    int widthH, heightH;
};


