/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "Averager.h"
#include "CircularBuffer.h"
#include "Goniometer.h"
#include "CorrelationMeter.h"
#include "PFM10LookAndFeel.h"

#define NEGATIVE_INFINITY -100.0f
#define MAX_DECIBELS 6.0f
#define REFRESH_RATE 60


extern bool showDecayingValueHolder;

struct Histogram : juce::Component
{
    void paint(juce::Graphics& g) override;
    void resized() override;
    void update(float);
    void setThreshold(float);

private:
    CircularBuffer<float> circBuffer;
    float threshold = 0;
    juce::ColourGradient colourGradient;
    juce::Path p;
};

struct HistogramContainer : juce::Component
{
    HistogramContainer()
    {
        addAndMakeVisible(histogramPeak);
        addAndMakeVisible(histogramRMS);
    }

    void resized() override
    {
        juce::FlexBox histFB;
        histFB.flexWrap = juce::FlexBox::Wrap::wrap;
        histFB.justifyContent = juce::FlexBox::JustifyContent::spaceBetween;
        histFB.items.add(juce::FlexItem(histogramPeak).withFlex(0.5).withMargin(6));
        histFB.items.add(juce::FlexItem(histogramRMS).withFlex(0.5).withMargin(6));

        histFB.flexDirection = (histStacked ? juce::FlexBox::Direction::column : juce::FlexBox::Direction::row);

        histFB.performLayout(getLocalBounds());
    }

    void updateLayout(bool view)
    {
        histStacked = view;
        resized();
    }

    Histogram histogramPeak, histogramRMS;
    bool histStacked {false};
};

struct DecayingValueHolder : juce::Timer
{  
    DecayingValueHolder(float _dr, int64 _ht) : currentValue(NEGATIVE_INFINITY), peakTime(0) { setDecayRate(_dr); setHoldTime(_ht), startTimerHz(REFRESH_RATE);}
    
    void updateHeldValue(float input);
    void setHoldTime(int ms);
    float getCurrentValue() const;
    void setDecayRate(float decayRatePerSecond);
    void timerCallback() override;
    void resetHeldValue();
private:
    float currentValue;
    float decayRate;
    int64 peakTime;
    int64 holdTime;
};

struct ValueHolder : juce::Timer
{  
    ValueHolder(float _th, float _hv) : heldValue(_hv), threshold(_th), timeOfPeak(juce::Time::currentTimeMillis()) { startTimer(REFRESH_RATE); }
    ~ValueHolder() { stopTimer(); }

    void setThreshold(float threshold);
    void updateHeldValue(float input);
    void setHoldTime(int ms);
    float getCurrentValue() const;    
    void timerCallback() override;
    float getHeldValue() const;
    bool getIsOverThreshold() const;
private:
    float currentValue = NEGATIVE_INFINITY;
    float heldValue = NEGATIVE_INFINITY;
    float threshold = 0;
    int durationToHoldForMs{ 500 };
    juce::int64 timeOfPeak;
    bool isOverThreshold{ false };
    void resetCurrentValue() { currentValue = threshold;}
};

struct TextMeter : juce::Component
{
    void paint(juce::Graphics& g) override;
    void update(float);
    ValueHolder valueHolder{ .0f, NEGATIVE_INFINITY };
private:
    float cachedValueDb;
    juce::String str;
};

struct Tick
{
    int y;
    float db;
};

struct DbScale : juce::Component
{
    void paint(juce::Graphics& g) override;
    std::vector<Tick> ticks;
    int yOffset = 0;
};
struct Meter : juce::Component
{
    void resized() override;
    void paint(juce::Graphics& g) override;
    void update(float);
    void setThreshold(float);
      
    DecayingValueHolder decayingValueHolder{ 60.f, 300 };
    float dbLevel = NEGATIVE_INFINITY;
    float threshold = 0.f;
    std::vector<Tick> ticks;
    ColourGradient colourGradient;
};

struct MeterLabel : juce::Component
{
    MeterLabel(juce::String _label) : label(_label)
    {
        label = "L " + label + " R";
    }
    void paint(juce::Graphics& g) override;
    juce::String label;
};

enum MeterPosition
{
    AverageOnLeft,
    AverageOnRight,
    PeakOnly,
    AverageOnly
};

struct MacroMeter : juce::Component
{
    MacroMeter(MeterPosition pos) : meterPosition(pos)
    {
        addAndMakeVisible(instanMeter);
        addAndMakeVisible(averageMeter);
        addAndMakeVisible(textMeter);
    }

    ~MacroMeter() {}

    MeterPosition meterPosition = AverageOnRight;
    void update(float);
    void resized() override;
    std::vector<Tick> getTicks();
    auto getMeterY()
    {
        return instanMeter.getY();
    }
    void setThreshold( float _tr)
    {
        textMeter.valueHolder.setThreshold(_tr);
        instanMeter.setThreshold(_tr);
        averageMeter.setThreshold(_tr);
    }

    void setHoldTime(int holdTime)
    {
        instanMeter.decayingValueHolder.setHoldTime(holdTime);
        averageMeter.decayingValueHolder.setHoldTime(holdTime);
    }

    void setDecayRate(float decayRate)
    {
        instanMeter.decayingValueHolder.setDecayRate(decayRate);
        averageMeter.decayingValueHolder.setDecayRate(decayRate);
    }

    void resetHeldValue()
    {
        instanMeter.decayingValueHolder.resetHeldValue();
        averageMeter.decayingValueHolder.resetHeldValue();
    }

    void hideAvg()
    {
        removeChildComponent(&averageMeter);
        addAndMakeVisible(&instanMeter);
    }
    
    void hidePeak()
    {
        removeChildComponent(&instanMeter);
        addAndMakeVisible(&averageMeter);
    }

    void restoreMeters()
    {
        addAndMakeVisible(&instanMeter);
        addAndMakeVisible(&averageMeter);
    }

    void resizeAverager(int size)
    {
        auto currentAverage = averager.getAverage();
        averager.resize(size, currentAverage);
    }

//private:
    TextMeter textMeter;
    Meter instanMeter;
    Meter averageMeter;
    Averager<float> averager{ 100, 0 };
    //int sliderValue = 0;
};

struct StereoMeter : juce::Component
{
    StereoMeter (juce::String name) : label(name)
    {
        addAndMakeVisible(dbScale);
        addAndMakeVisible(meterLeft);
        addAndMakeVisible(meterRight);
        addAndMakeVisible(meterLabel);
        addAndMakeVisible(slider);

        slider.setRange (NEGATIVE_INFINITY, MAX_DECIBELS);

        //slider.setLookAndFeel(&lnf);
        slider.setColour(Slider::ColourIds::backgroundColourId, juce::Colours::transparentWhite);
        slider.setColour(Slider::ColourIds::trackColourId, juce::Colours::transparentWhite);
        //slider.hideTextBox(0);
    }

    juce::String label;
    DbScale dbScale;
    MacroMeter meterLeft { MeterPosition::AverageOnRight };
    MacroMeter meterRight { MeterPosition::AverageOnLeft };
    MeterLabel meterLabel { label };

    //PFM10LookAndFeel lnf;
    juce::Slider slider { juce::Slider::LinearVertical, juce::Slider::NoTextBox };

    void setDecayRate(float decayRate)
    {
        meterLeft.setDecayRate(decayRate);
        meterRight.setDecayRate(decayRate);
    }

    void setHoldTime(int holdTime)
    {
        meterLeft.setHoldTime(holdTime);
        meterRight.setHoldTime(holdTime);
    }

    void resetHeldValue()
    {
        meterLeft.resetHeldValue();
        meterRight.resetHeldValue();
    }

    void hideAvg()
    {
        meterLeft.hideAvg();
        meterRight.hideAvg();
        meterLeft.meterPosition = PeakOnly;
        meterRight.meterPosition = PeakOnly;
        meterLeft.resized();
        meterRight.resized();
    }

    void hidePeak()
    {
        meterLeft.hidePeak();
        meterRight.hidePeak();
        meterLeft.meterPosition = AverageOnly;
        meterRight.meterPosition = AverageOnly;
        meterLeft.resized();
        meterRight.resized();
    }

    void restoreMeters()
    {
        meterLeft.restoreMeters();
        meterRight.restoreMeters();
        meterLeft.meterPosition = AverageOnRight;
        meterRight.meterPosition = AverageOnLeft;
        meterLeft.resized();
        meterRight.resized();
    }

    void resizeAverager(int size)
    {
        meterLeft.resizeAverager(size);
        meterRight.resizeAverager(size);
    }

    void paint(juce::Graphics& g) override;
    void update(float left, float right);
    void resized() override;
};

struct StereoImageMeter : juce::Component
{
    StereoImageMeter(double sampleRate) : correlationMeter(sampleRate)
    {
        addAndMakeVisible(goniometer);
        addAndMakeVisible(correlationMeter);
    }
    
    void paint(juce::Graphics& g) override;
    void update(const juce::AudioBuffer<float>& buffer);
    void resized() override;
    Goniometer goniometer;
    
private:
    CorrelationMeter correlationMeter;
};

struct WarningMessage : juce::Component
{
    void paint(juce::Graphics& g) override;
    void mouseDown(const juce::MouseEvent& event) override;
};


//==============================================================================
/**
*/
class Pfmcpp_project10AudioProcessorEditor  : public AudioProcessorEditor, juce::Timer, public juce::Value::Listener
{
public:
    Pfmcpp_project10AudioProcessorEditor(Pfmcpp_project10AudioProcessor& p);
    ~Pfmcpp_project10AudioProcessorEditor();
    
    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    
    void timerCallback() override;
   
private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Pfmcpp_project10AudioProcessor& processor;

    StereoMeter meterPeak{ "Peak" };
    StereoMeter meterRMS{ "RMS" };

    HistogramContainer histBox;

    StereoImageMeter stereoImageMeter;

    juce::ComboBox decayRateCB;

    void decayRateChanged()
    {
        auto decayRate = decayRateCB.getSelectedId();
        meterPeak.setDecayRate(decayRate);
        meterRMS.setDecayRate(decayRate);
    }

    juce::ComboBox metersSelect;

    void metersSelectChanged()
    {
        switch (metersSelect.getSelectedId())
        {
            case 1: meterPeak.restoreMeters(); meterRMS.restoreMeters(); break;
            case 2: meterPeak.hideAvg(); meterRMS.hideAvg(); break;
            case 3: meterPeak.hidePeak(); meterRMS.hidePeak(); break;
        }

    }

    juce::ComboBox holdDuration;

    void holdDurationChanged()
    {
        int holdTime = 0;
        removeChildComponent(&resetHold);
        switch (holdDuration.getSelectedId())
        {
            case 1: holdTime = 0; meterPeak.setDecayRate(60); meterRMS.setDecayRate(60); break;
            case 2: holdTime = 500; break;
            case 3: holdTime = 2000; break;
            case 4: holdTime = 4000; break;
            case 5: holdTime = 6000; break;
            case 6: holdTime = -1; addAndMakeVisible(&resetHold); break;
        }
        meterPeak.setHoldTime(holdTime);
        meterRMS.setHoldTime(holdTime);
    }

    juce::TextButton holdButton {"Peak Holders"};

    juce::TextButton resetHold {"reset"};
    void resetHoldClicked()
    {
        meterPeak.resetHeldValue();
        meterRMS.resetHeldValue();
    }

    juce::ComboBox averagerDuration;
    void averagerDurationChanged()
    {
        auto size = averagerDuration.getSelectedId();
        meterPeak.resizeAverager(size);
        meterRMS.resizeAverager(size);
    }

    juce::Value editorHoldButtonValue;

    void valueChanged(Value& value) override
    {

        if (value == holdButton.getToggleStateValue())
        {
            showDecayingValueHolder = value.getValue();
        }
        if (value == histView.getToggleStateValue())
        {
            auto view = histView.getToggleState();
            histView.setButtonText( view ? "Stacked" : "Side-By-Side" );
            histBox.updateLayout(view);
            histBox.resized();
        }
    }

    juce::Slider zoom{juce::Slider::Rotary, juce::Slider::NoTextBox};

    juce::TextButton histView {"Side-by-Side"};

    juce::ComboBox themeSelect;

    juce::Label decayRateLabel {"decayRateLabel", "Decay"};
    juce::Label averagerDurationLabel {"averagerDurationLabel", "Averager"};
    juce::Label holdDurationLabel {"holdDurationLabel", "Hold"};
    juce::Label zoomLabel {"zoomLabel", "Scale"};

    juce::Label metersLabel{"metersLabel", "Meters"};
    juce::Label histViewLabel{"histViewLabel", "Histogram"};
    juce::Label peakLabel{"peakLabel", "Peaks"};
    juce::Label themeLabel{"themeLabel", "Theme"};

    WarningMessage warningMessage;


    float thresholdPeak = 0.f;
    float thresholdRMS = 0.f;

    PFM10LookAndFeel lnf;
    
    AudioBuffer<float> buffer;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Pfmcpp_project10AudioProcessorEditor)
};
