/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Pfmcpp_project10AudioProcessor::Pfmcpp_project10AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif

{
    static juce::Identifier PFM10GlobalTree("PFM10GlobalTree");
    PFM10ValueTree = juce::ValueTree(PFM10GlobalTree);

    const juce::Identifier pluginVersion("pluginVersion"), decayRateValue("decayRateValue"),  metersSelectValue("metersSelectValue"),
    sliderPeakValue("sliderPeakValue"), sliderRMSValue("sliderRMSValue"), holdButtonValue("holdButtonValue"), holdDurationValue("holdDurationValue"),averagerDurationValue("averagerDurationValue"), zoomValue("zoomValue"),
    histViewValue("histViewValue"), themeSelectValue("themeSelectValue");

    PFM10ValueTree.setProperty("pluginVersion", pluginVersionString, nullptr);
    PFM10ValueTree.setProperty("decayRateValue", "6", nullptr);
    PFM10ValueTree.setProperty("metersSelectValue", "1", nullptr);
    PFM10ValueTree.setProperty("holdButtonValue", "0", nullptr);
    PFM10ValueTree.setProperty("holdDurationValue", "2", nullptr);
    PFM10ValueTree.setProperty("averagerDurationValue", "500", nullptr);
    PFM10ValueTree.setProperty("zoomValue", "1.0", nullptr);
    PFM10ValueTree.setProperty("histViewValue", "0", nullptr);
    PFM10ValueTree.setProperty("sliderPeakValue", "0", nullptr);
    PFM10ValueTree.setProperty("sliderRMSValue", "0", nullptr);
    PFM10ValueTree.setProperty("themeSelectValue", "2", nullptr);
}

Pfmcpp_project10AudioProcessor::~Pfmcpp_project10AudioProcessor()
{
}


//==============================================================================
const String Pfmcpp_project10AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Pfmcpp_project10AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Pfmcpp_project10AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Pfmcpp_project10AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double Pfmcpp_project10AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Pfmcpp_project10AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Pfmcpp_project10AudioProcessor::getCurrentProgram()
{
    return 0;
}

void Pfmcpp_project10AudioProcessor::setCurrentProgram (int index)
{
}

const String Pfmcpp_project10AudioProcessor::getProgramName (int index)
{
    return {};
}

void Pfmcpp_project10AudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Pfmcpp_project10AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..

    fifo.prepare(samplesPerBlock, getTotalNumOutputChannels());
    numSamples = samplesPerBlock;

#if defined(WITH_DBSCALE)
    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;
    spec.sampleRate = sampleRate;
    spec.numChannels = getTotalNumOutputChannels();
    
    osc.prepare(spec);
    gainDb.prepare(spec);
    
    osc.setFrequency(440.0f);
    gainDb.setGainDecibels(-12);
#endif
    
}

void Pfmcpp_project10AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Pfmcpp_project10AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void Pfmcpp_project10AudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        auto* channelData = buffer.getWritePointer(channel);
    }

#if defined(WITH_DBSCALE)
        juce::dsp::AudioBlock<float> audioBlock { buffer };
        for (int sampleIndex = 0; sampleIndex < buffer.getNumSamples(); ++sampleIndex)
        {
            auto sample = osc.processSample(0);
            for (int channel = 0; channel < totalNumOutputChannels; ++channel)
            {
                buffer.setSample(channel, sampleIndex, sample);
            }
        }
        gainDb.process(juce::dsp::ProcessContextReplacing<float>(audioBlock));
#endif
    fifo.push(buffer);
}

//==============================================================================
bool Pfmcpp_project10AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Pfmcpp_project10AudioProcessor::createEditor()
{
    return new Pfmcpp_project10AudioProcessorEditor (*this);
}

//==============================================================================
void Pfmcpp_project10AudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.

    PFM10ValueTree.setProperty("pluginVersion", pluginVersionString, nullptr);
    auto memStream = juce::MemoryOutputStream(destData, false);
    PFM10ValueTree.writeToStream(memStream);
}
void Pfmcpp_project10AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.

    auto memStream = juce::MemoryInputStream(data, sizeInBytes, false);
    auto tree = juce::ValueTree::readFromStream(memStream);

    auto ver = tree.getPropertyAsValue("pluginVersion", nullptr).getValue().toString();
    auto pvs = juce::String(pluginVersionString);
    DBG("save version:" + ver);
    DBG("plugin version:" + pvs);

    if (pvs > ver)
    {
        DBG("save is older");
        oldversionstate = true;
    }

    if(tree.isValid() && tree.hasProperty("decayRateValue"))
    {
        PFM10ValueTree.copyPropertiesAndChildrenFrom(tree, nullptr);
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Pfmcpp_project10AudioProcessor();
}
