#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

template<typename T>
struct Averager
{
    Averager(size_t numElements, T initialValue)
    {
        resize(numElements, initialValue);
    }

    void clear(T initialValue)
    {
        averagedVector.assign(getSize(), initialValue);
        sum = initialValue * getSize();
        atomAverage.store(initialValue);
    }

    void resize(size_t s, T initialValue)
    {
        averagedVector.resize(s, initialValue);
        clear(initialValue);
        atomIndex = 0;
    }

    void add(T t)
    {
        auto localIndex = atomIndex.load();
        auto size = averagedVector.size();
        sum = sum - averagedVector[localIndex] + t;
        averagedVector[localIndex] = t;
        ++localIndex;
        if (localIndex == size)
        {
            localIndex = 0;
        }
        atomIndex.store(localIndex);
        atomAverage.store(static_cast<float>(sum / size));
    }

    size_t getSize() const
    {
        return averagedVector.size();
    }

    float getAverage()
    {
        return atomAverage.load();
    }
private:
    std::vector<T> averagedVector;
    T sum = 0;
    std::atomic<size_t> atomIndex{ 0 };
    std::atomic<T> atomAverage;
};