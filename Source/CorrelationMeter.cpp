/*
  ==============================================================================

    CorrelationMeter.cpp
    Created: 1 Sep 2021 3:15:14pm
    Author:  kvoinov

  ==============================================================================
*/

#include "CorrelationMeter.h"

void CorrelationMeter::resized()
{
    bounds = getLocalBounds();
    x = bounds.getCentreX();
    yInst = getHeight() - 20;
    yAver = yInst - 12;
    w = bounds.getWidth() / 2 - 22;
    
//     DBG (getHeight() << " " << yInst);
}

void CorrelationMeter::paint(juce::Graphics& g)
{
    auto valueAverA = correlationAveragerA.getAverage();
    auto valueAverB = correlationAveragerB.getAverage();
    
    g.setColour (findColour(PFM10LookAndFeel::stripeColourId));
    g.setFont(18);
    g.drawFittedText("-1", 2, yInst, 20, 20, juce::Justification::horizontallyJustified, 1.0f);
    g.drawFittedText("+1", bounds.getWidth() - 20, yInst, 20, 20, juce::Justification::horizontallyJustified, 1.0f);

    colourGradient = ColourGradient(juce::Colours::orange, 0.f, static_cast<float>(yInst), juce::Colours::green, static_cast<float>(bounds.getWidth()), static_cast<float>(yInst), false);
    g.setGradientFill(colourGradient);
    
    int aNorm = static_cast<int>(juce::jmap(valueAverA, -1.f, 1.f, -1 * static_cast<float>(w), static_cast<float>(w)));
    if (aNorm < 0)
        g.fillRect(x + aNorm, yInst, abs(aNorm), 20);
    else
        g.fillRect(x, yInst, aNorm, 20);

    int bNorm = static_cast<int>(juce::jmap(valueAverB, -1.f, 1.f, -1 * static_cast<float>(w), static_cast<float>(w)));
    if (bNorm < 0)
        g.fillRect(x + bNorm, yAver, abs(bNorm), 6);
    else
        g.fillRect(x, yAver, bNorm, 6);
    
}

void CorrelationMeter::update(const juce::AudioBuffer<float>& buffer)
{
    for (int i = 0; i < buffer.getNumSamples(); ++i)
    {
        auto left = buffer.getSample(0, i);
        auto right = buffer.getSample(1, i);
        auto denominator = sqrt(filterL.processSample(left * left) * filterR.processSample(right * right));
        if (denominator == 0 || isinf(denominator) || isnan(denominator))
        {
            correlationAveragerA.add(0);
            correlationAveragerB.add(0);
        }
        else
        {
            correlationAveragerA.add(filterC.processSample(left * right) / denominator);
            correlationAveragerB.add(filterC.processSample(left * right) / denominator);            
        }

        //DBG(cInst << " " << correlationAverager.getAverage());
    }
    repaint();
}
