/*
 *  ==============================================================================
 * 
 *    This file was auto-generated!
 * 
 *    It contains the basic framework code for a JUCE plugin editor.
 * 
 *  ==============================================================================
 */

#include "PluginProcessor.h"
#include "PluginEditor.h"


bool showDecayingValueHolder = true;

void Histogram::setThreshold(float th)
{
    threshold = th;
}

void Histogram::paint(juce::Graphics& g)
{
    auto bounds = getBounds();
    auto height = static_cast<float>(getHeight());
    auto pBottomRight = bounds.getBottomRight().toFloat();
    auto pBottomLeft = bounds.getBottomLeft().toFloat();
    colourGradient = ColourGradient::vertical(findColour(PFM10LookAndFeel::meterMinColourId), height, findColour(PFM10LookAndFeel::meterMaxColourId), 0);
    double thresholdInv = jmap(threshold, NEGATIVE_INFINITY, MAX_DECIBELS, 0.0f, 0.98f);
    auto& data = circBuffer.getData();
    auto size = circBuffer.getSize();
    int index = circBuffer.getReadIndex();
    int x = 0.f;

    p.clear();

    for (auto &i : data)
    {
        auto y = jmap(data[index], NEGATIVE_INFINITY, MAX_DECIBELS, height, 0.f);
        if (x == 0.f)
            p.startNewSubPath(0.f, y);
        else
            p.lineTo(x, y);

        ++index;
        if (index >= size)
        {
            index = 0;
        }
        ++x;
    }
    p.lineTo(pBottomRight);
    p.lineTo(pBottomLeft.withX(0));
    
    p.closeSubPath();

    auto sampledColor = colourGradient.getColourAtPosition(thresholdInv);

    colourGradient.clearColours();
    colourGradient.addColour(0, findColour(PFM10LookAndFeel::meterMinColourId).withAlpha(0.9f));
    colourGradient.addColour(thresholdInv, sampledColor.withAlpha(0.9f));
    colourGradient.addColour(thresholdInv + 0.01, juce::Colour(164,41,41).withAlpha(0.9f));
    colourGradient.addColour(1, juce::Colour(164,41,41).withAlpha(0.9f));

    g.setGradientFill(colourGradient);
    
    g.fillPath(p);


    int heightInt = getHeight();
    int width = getWidth();

    g.setColour(findColour(PFM10LookAndFeel::stripeColourId));
    int step = heightInt / 4;
    auto halfstep = step / 2;
    auto length = step * (width / step);
//    int ticknum = 0;
//    g.setFont(height / 8);

    for (int i = halfstep; i <= heightInt; i=i+step)
    {
        g.fillRect(halfstep, i, length, 1);
//        g.drawSingleLineText(ticks[ticknum], halfstep, i, juce::Justification::horizontallyCentred);
//        ++ticknum;
    }
    for (int i = step; i < width; i=i+step)
    {
        g.fillRect(i, 0, 1, heightInt);
    }
}

void Histogram::update(float data)
{
    circBuffer.write(data);
    repaint();
}

void Histogram::resized()
{
    circBuffer.resize(getWidth(), NEGATIVE_INFINITY);
}

void MacroMeter::update(float gainLevel)
{
    textMeter.update(gainLevel);
    instanMeter.update(gainLevel);
    averager.add(gainLevel);
    averageMeter.update(averager.getAverage());
}

void MacroMeter::resized()
{
    auto width = getWidth();
    auto height = getHeight();

    textMeter.setBounds( width / 4, 0, 32 , 16 );

    if (meterPosition == MeterPosition::AverageOnRight)
    {
        instanMeter.setBounds(0, textMeter.getBottom() + 5, width / 2, height);
        averageMeter.setBounds(instanMeter.getRight() + 4, textMeter.getBottom() + 5 , width / 4, height);
    }
    if (meterPosition == MeterPosition::AverageOnLeft)
    {
        averageMeter.setBounds(0, textMeter.getBottom() + 5, width / 4, height);
        instanMeter.setBounds(averageMeter.getRight() + 4, textMeter.getBottom() + 5, width / 2, height);
    }
    if (meterPosition == MeterPosition::PeakOnly)
    {
        instanMeter.setBounds(0, textMeter.getBottom() + 5, width / 4 * 3 + 4, height);
    }
    if (meterPosition == MeterPosition::AverageOnly)
    {
        averageMeter.setBounds(0, textMeter.getBottom() + 5, width / 4 * 3 + 4, height);
    }
}

std::vector<Tick> MacroMeter::getTicks()
{
    return instanMeter.ticks;
}

void TextMeter::update(float input)
{
    cachedValueDb = input;
    valueHolder.updateHeldValue(cachedValueDb);
    repaint();
}

void TextMeter::paint(juce::Graphics& g)
{
    juce::Colour textColor;
    float valueToDisplay = 0.f;

    if (valueHolder.getIsOverThreshold())
    {
        textColor = juce::Colours::black;
        valueToDisplay = valueHolder.getHeldValue();;
        g.fillAll(juce::Colours::red);
    }
    else
    {
        textColor = juce::Colours::white;
        valueToDisplay = valueHolder.getCurrentValue();
        g.fillAll(juce::Colours::darkgrey);
    }

    g.setFont(12);

    if (valueToDisplay <= NEGATIVE_INFINITY)
    {
        str = "-inf";
        g.setColour(juce::Colours::grey);
    }
    
    else
    {
        str = String(valueToDisplay, 1);
        g.setColour(textColor);
    }
    
    //g.setFont(getWidth()/3);
    //g.drawSingleLineText(str, 0, Justification::centred, 1);
    g.drawFittedText(str, 4, 0, 24, 16, Justification::centred, 1);
}

float DecayingValueHolder::getCurrentValue() const
{
    return currentValue;
}

void DecayingValueHolder::setDecayRate(float decayRatePerSecond)
{
    decayRate = decayRatePerSecond / REFRESH_RATE;
}

void DecayingValueHolder::updateHeldValue(float input)
{
    if (holdTime == 0)
        currentValue = input;
    else if (input > currentValue)
    {
        peakTime = juce::Time::currentTimeMillis();
        currentValue = input;
    }
}

void DecayingValueHolder::setHoldTime(int ms)
{
    holdTime = ms;
}

void DecayingValueHolder::timerCallback()
{
    if(holdTime != -1 && holdTime != 0 && juce::Time::currentTimeMillis() - peakTime > holdTime)
    {
        if (currentValue > NEGATIVE_INFINITY)
        {
            currentValue = currentValue - decayRate;
        }
        else
        {
            currentValue = NEGATIVE_INFINITY;
        }
    }   
}

void DecayingValueHolder::resetHeldValue()
{
    currentValue = NEGATIVE_INFINITY;
}

void ValueHolder::setThreshold(float th)
{
    threshold = th;
    isOverThreshold = (currentValue > threshold);
}

float ValueHolder::getCurrentValue() const
{
    return currentValue;
}

void ValueHolder::updateHeldValue(float input)
{
    if (input > threshold)
    {
        isOverThreshold = true;
        timeOfPeak = juce::Time::currentTimeMillis();
        if (input > heldValue)
            heldValue = input;
    }

    currentValue = input;
}

void ValueHolder::setHoldTime(int ms)
{
    durationToHoldForMs  = ms;
}

bool ValueHolder::getIsOverThreshold() const
{
    return isOverThreshold;
}

float ValueHolder::getHeldValue() const
{
    return heldValue;
}

void ValueHolder::timerCallback()
{
    auto now = juce::Time::currentTimeMillis();
    auto elapsed = now - timeOfPeak;

    if(elapsed > durationToHoldForMs)
    {
        isOverThreshold = (currentValue > threshold);
    }
    
    if (getIsOverThreshold() == false)
    {
        heldValue = NEGATIVE_INFINITY;
    }
}

void MeterLabel::paint(juce::Graphics& g)
{
    g.setColour(findColour(juce::Label::textColourId));
    g.setFont(getHeight());
    g.drawFittedText(label, 0, 0, getWidth(), getHeight(), juce::Justification::horizontallyJustified, 1.f);
}

void DbScale::paint(juce::Graphics& g)
{
    auto width = getWidth();
    auto height = getHeight();
    auto center = width / 2 ;
    g.setColour(findColour(juce::Label::textColourId));
    g.setFont(14);
    for (auto t : ticks)
    {
        g.drawSingleLineText(juce::String(t.db), center, t.y + yOffset, juce::Justification::horizontallyCentred);

        g.fillRect(0, t.y+yOffset, 10, 1);
        g.fillRect(center + 20, t.y+yOffset, center + 30, 1);

        for(int i=yOffset + 6; i <= yOffset + 18; i=i+6)
        {
            g.fillRect(0, t.y+i, 5, 1);
            g.fillRect(center + 25, t.y+i, center + 30, 1);
        }
    }
}

void Meter::setThreshold(float _th)
{
    threshold = _th;
}

void Meter::update(float input)
{
    dbLevel = input;
    decayingValueHolder.updateHeldValue(dbLevel);
    repaint();
}

void Meter::resized()
{
    auto bounds = getLocalBounds();
    auto h = bounds.getHeight();

    ticks.clear();
    for (int i = NEGATIVE_INFINITY; i <= MAX_DECIBELS; ++i)
    {
        if (i % 6 == 0)
        {
            Tick t;
            t.db = i;
            t.y = juce::jmap(static_cast<int>(i), static_cast<int>(NEGATIVE_INFINITY), static_cast<int>(MAX_DECIBELS), h, 0);
            ticks.push_back(t);
        }
    }
}

void Meter::paint(juce::Graphics& g)
{
    auto bounds = getLocalBounds();
    auto h = bounds.getHeight();
    auto w = bounds.getWidth();
    colourGradient = ColourGradient(findColour(PFM10LookAndFeel::meterMinColourId), static_cast<float>(w / 2), static_cast<float>(h), findColour(PFM10LookAndFeel::meterMaxColourId), static_cast<float>(w / 2), 0, false);
    auto dbLevelNorm = jmap(static_cast<int>(dbLevel), static_cast<int>(NEGATIVE_INFINITY), static_cast<int>(MAX_DECIBELS), h, 0);
    auto thresholdNorm = jmap(static_cast<int>(threshold), static_cast<int>(NEGATIVE_INFINITY), static_cast<int>(MAX_DECIBELS), h, 0);
    g.setGradientFill(colourGradient);

    if (dbLevel > threshold)
    {
        g.fillRect(bounds.withHeight(h - thresholdNorm).withY(thresholdNorm));
        g.setColour(juce::Colour(164,41,41));
        g.fillRect(bounds.withHeight(thresholdNorm - dbLevelNorm).withY(dbLevelNorm));
    }
    else
    {
        g.fillRect(bounds.withHeight(h - dbLevelNorm).withY(dbLevelNorm));
    }

    if (showDecayingValueHolder == true)
    {
        auto dbDecayingLevelNorm = jmap(static_cast<int>(decayingValueHolder.getCurrentValue()), static_cast<int>(NEGATIVE_INFINITY), static_cast<int>(MAX_DECIBELS), h, 0);

        if (decayingValueHolder.getCurrentValue() > threshold)
            g.setColour(juce::Colours::darkred);
        else
            g.setColour(juce::Colour(26,157,200));

        g.fillRect(bounds.withHeight(2).withY(dbDecayingLevelNorm));
    }
}

void StereoMeter::paint(juce::Graphics& g)
{
     //g.fillAll(juce::Colours::black);
}


void StereoMeter::resized()
{
    //auto offset1 = JUCE_LIVE_CONSTANT(10);
    //auto offset2 = JUCE_LIVE_CONSTANT(440);
    auto width = getWidth();
    auto height = getHeight();
    meterLeft.setBounds(0, 0, 70, height - 40);

    dbScale.ticks = meterLeft.getTicks();
    dbScale.yOffset = meterLeft.getMeterY();
    dbScale.setBounds(meterLeft.getRight(), 0, 60, height - dbScale.yOffset);

    meterRight.setBounds(dbScale.getRight() + 15, 0, 70, height - 40);
//    auto meterWidth = meterLeft.getWidth() + dbScale.getWidth() + meterRight.getWidth();
    meterLabel.setBounds(64, dbScale.getBottom(), 80, 20);
    slider.setBounds(dbScale.getX() , 10, dbScale.getWidth(), 422);
}

void StereoMeter::update(float left, float right)
{
    meterLeft.update(left);
    meterRight.update(right);
}

void StereoImageMeter::resized()
{
    goniometer.setBounds(0, 0, getWidth(), getHeight() / 1.05);
    correlationMeter.setBounds(0, getHeight() / 1.15, getWidth(), getHeight() / 9.5);
}

void StereoImageMeter::paint(juce::Graphics& g)
{
    g.fillAll(findColour(PFM10LookAndFeel::goniGroundId));
        
}

void StereoImageMeter::update(const juce::AudioBuffer<float>& buffer)
{
    goniometer.update(buffer);
    correlationMeter.update(buffer);
}

void WarningMessage::paint(juce::Graphics& g)
{
    g.fillAll(juce::Colours::lightgrey);
    g.setColour(juce::Colours::black);
    g.setFont(24);
    g.drawFittedText("The project has an old version save, savings with current version may not work in old PFMCPP_Project10.",
                     getLocalBounds(),juce::Justification::centred,5,1.0f);
}
void WarningMessage::mouseDown(const juce::MouseEvent& event)
{
    getParentComponent()->removeChildComponent(this);
}

void Pfmcpp_project10AudioProcessorEditor::timerCallback()
{
    if (processor.fifo.pull(buffer))
    {
        auto RMSLevelLeft = juce::Decibels::gainToDecibels(buffer.getRMSLevel(0, 0, buffer.getNumSamples()));
        auto RMSLevelRight = juce::Decibels::gainToDecibels(buffer.getRMSLevel(1, 0, buffer.getNumSamples()));
        auto gainLevelLeft = juce::Decibels::gainToDecibels(buffer.getMagnitude(0, 0, buffer.getNumSamples()));
        auto gainLevelRight = juce::Decibels::gainToDecibels(buffer.getMagnitude(1, 0, buffer.getNumSamples()));
        auto histLevelPeakMono = (gainLevelLeft + gainLevelRight) * 0.5f;
        auto histLevelRMSMono = (RMSLevelLeft + RMSLevelRight) * 0.5f;

        meterRMS.update(RMSLevelLeft, RMSLevelRight);
        meterPeak.update(gainLevelLeft, gainLevelRight);
        histBox.histogramPeak.update(histLevelPeakMono);
        histBox.histogramRMS.update(histLevelRMSMono);
        stereoImageMeter.update(buffer);
    }
}
//==============================================================================
Pfmcpp_project10AudioProcessorEditor::Pfmcpp_project10AudioProcessorEditor (Pfmcpp_project10AudioProcessor& p)
: AudioProcessorEditor (&p), processor (p), stereoImageMeter(processor.getSampleRate())

{
    buffer.setSize(processor.getTotalNumOutputChannels(), processor.numSamples, true, true, true);
    buffer.clear();
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.

    startTimerHz(REFRESH_RATE);

    setResizable(false, false);
    setLookAndFeel(&lnf);

    decayRateCB.addItem("-3dB/s",  3);
    decayRateCB.addItem("-6dB/s",  6);
    decayRateCB.addItem("-12dB/s", 12);
    decayRateCB.addItem("-24dB/s", 24);
    decayRateCB.addItem("-36dB/s", 36);
    decayRateCB.addItem("-60dB/s", 60);
    decayRateCB.onChange = [this] {decayRateChanged();};
    decayRateCB.getSelectedIdAsValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("decayRateValue", nullptr));

    metersSelect.addItem("Both",  1);
    metersSelect.addItem("Peak",  2);
    metersSelect.addItem("Avg", 3);
    metersSelect.onChange = [this] {metersSelectChanged();};
    metersSelect.getSelectedIdAsValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("metersSelectValue", nullptr));

    holdButton.setClickingTogglesState(true);
    holdButton.getToggleStateValue().addListener(this);
    holdButton.getToggleStateValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("holdButtonValue", nullptr));

    holdDuration.addItemList({"0s", "0.5s", "2s", "4s", "6s", "inf"}, 1);
    holdDuration.onChange = [this] {holdDurationChanged();};
    holdDuration.getSelectedIdAsValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("holdDurationValue", nullptr));

    resetHold.onClick = [this] {resetHoldClicked();};

    averagerDuration.addItem("100ms", 100);
    averagerDuration.addItem("250ms", 250);
    averagerDuration.addItem("500ms", 500);
    averagerDuration.addItem("1000ms", 1000);
    averagerDuration.addItem("2000ms", 2000);
    averagerDuration.onChange = [this]  {averagerDurationChanged();};
    averagerDuration.getSelectedIdAsValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("averagerDurationValue", nullptr));

    zoom.setRange(.5, 2);
    //zoom.setValue(1, sendNotification);
    zoom.setDoubleClickReturnValue(true, 1);
    zoom.onValueChange = [&]()
    {
        auto val = zoom.getValue();
        stereoImageMeter.goniometer.gmeterZoom(val);
    };
    zoom.getValueObject().referTo(processor.PFM10ValueTree.getPropertyAsValue("zoomValue", nullptr));


    histView.setClickingTogglesState(true);
    histView.getToggleStateValue().addListener(this);
    histView.getToggleStateValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("histViewValue", nullptr));

    meterPeak.slider.setValue(processor.PFM10ValueTree.getProperty("sliderPeakValue", 0.0), sendNotification);
    meterPeak.slider.onValueChange = [&]()
    {
        auto val = meterPeak.slider.getValue();
        meterPeak.meterLeft.setThreshold(val);
        meterPeak.meterRight.setThreshold(val);
        histBox.histogramPeak.setThreshold(val);
    };

    meterRMS.slider.setValue(processor.PFM10ValueTree.getProperty("sliderRMSValue", 0.0), sendNotification);
    meterRMS.slider.onValueChange = [&]()
    {
        auto val = meterRMS.slider.getValue();
        meterRMS.meterLeft.setThreshold(val);
        meterRMS.meterRight.setThreshold(val);
        histBox.histogramRMS.setThreshold(val);
    };

    meterPeak.slider.getValueObject().referTo(processor.PFM10ValueTree.getPropertyAsValue("sliderPeakValue", nullptr));
    meterRMS.slider.getValueObject().referTo(processor.PFM10ValueTree.getPropertyAsValue("sliderRMSValue", nullptr));

    themeSelect.addItem("dark", 2);
    themeSelect.addItem("light", 1);
    themeSelect.onChange = [this]
    {
        auto val = themeSelect.getSelectedId();
        lnf.switchTheme(val);
        sendLookAndFeelChange();
    };
    themeSelect.getSelectedIdAsValue().referTo(processor.PFM10ValueTree.getPropertyAsValue("themeSelectValue", nullptr));

    addAndMakeVisible(meterPeak);
    addAndMakeVisible(meterRMS);
    addAndMakeVisible(stereoImageMeter);
    addAndMakeVisible(histBox);
    addAndMakeVisible(decayRateCB);
    addAndMakeVisible(metersSelect);
    addAndMakeVisible(holdButton);
    addAndMakeVisible(holdDuration);
    addAndMakeVisible(decayRateLabel);
    addAndMakeVisible(resetHold);
    addAndMakeVisible(averagerDuration);
    addAndMakeVisible(zoom);
    addAndMakeVisible(histView);
    addAndMakeVisible(themeSelect);
    addAndMakeVisible(themeLabel);
    addAndMakeVisible(averagerDurationLabel);
    addAndMakeVisible(holdDurationLabel);
    addAndMakeVisible(zoomLabel);
    addAndMakeVisible(metersLabel);
    metersLabel.setJustificationType(juce::Justification::right);
    addAndMakeVisible(histViewLabel);
    histViewLabel.setJustificationType(juce::Justification::right);

    setSize(800, 600);

    if(processor.oldversionstate)
        addAndMakeVisible(warningMessage);
}

Pfmcpp_project10AudioProcessorEditor::~Pfmcpp_project10AudioProcessorEditor()
{
    setLookAndFeel(nullptr);
    stopTimer();
}

//==============================================================================
void Pfmcpp_project10AudioProcessorEditor::paint (Graphics& g)
{
    
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
    //g.fillAll (juce::Colour(25,28,42));
    
}

void Pfmcpp_project10AudioProcessorEditor::resized()
{
    auto height = getHeight();
    auto width = getWidth();
    
    meterPeak.setBounds(5, 5, 205, 440);

    //auto offsetR = JUCE_LIVE_CONSTANT(60);
    //auto offsetY = JUCE_LIVE_CONSTANT(5);
    auto offsetY = 8;
    meterRMS.setBounds(587,5,205,440);

    histBox.setBounds(20, 485, 760, 110);
    stereoImageMeter.setBounds(width / 3.64, height / 40, width / 2.22, height / 1.58);

    decayRateCB.setBounds(stereoImageMeter.getX(), stereoImageMeter.getBottom() + offsetY, 80, 20);
    averagerDuration.setBounds(decayRateCB.getX(), decayRateCB.getBottom() + offsetY, 80, 20);
    holdDuration.setBounds(averagerDuration.getX(), averagerDuration.getBottom() + offsetY, 60, 20);

    metersSelect.setBounds(stereoImageMeter.getRight() - 80, stereoImageMeter.getBottom() + offsetY, 80, 20);
    histView.setBounds(metersSelect.getX(), metersSelect.getBottom() + offsetY, 80, 20);
    holdButton.setBounds(histView.getRight() - 80, histView.getBottom() + offsetY, 80, 20);

    zoom.setBounds(365, stereoImageMeter.getBottom(), 70, 70);
    zoomLabel.setBounds(zoom.getBounds().getCentreX() - 23, zoom.getBottom() - 10, 50, 20);

    decayRateLabel.setBounds(decayRateCB.getRight(), decayRateCB.getY(), 60, 20);
    averagerDurationLabel.setBounds(averagerDuration.getRight(), averagerDuration.getY(), 60, 20);
    holdDurationLabel.setBounds(holdDuration.getRight(), holdDuration.getY(), 40, 20);

    metersLabel.setBounds(metersSelect.getX() - 60, metersSelect.getY(), 60, 20);
    histViewLabel.setBounds(histView.getX() - 60, histView.getY(), 60, 20);
    //peakLabel.setBounds(holdButton.getX() - 40, holdButton.getY(), 40, 20);

    resetHold.setBounds(holdDurationLabel.getRight(), holdDurationLabel.getY(), 40, 20);

    themeSelect.setBounds(width - 85, holdButton.getY(), 80, 20);
    themeLabel.setBounds(themeSelect.getX() - 60, themeSelect.getY(), 60, 20);

    warningMessage.setBounds(160, 160, 480, 280);

    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
}
